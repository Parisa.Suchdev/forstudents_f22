// jdh CS201 Fall 2022
//
// 1. change the include statement to reference your .h file
// 2. compile this with your own warmup.netid.c
// 3. run the resulting program

#include <stdio.h>
#include <string.h>
#include "warmup.jhibbele.h"

int test_checkChars() {
  const char *testName = __FUNCTION__;
  char *s = "now is the time for all good men";
  int rc, result, errCount, len;

  errCount = 0;

  len = strlen(s);

  // this should pass, and result should be 1
  rc = checkChars(s, 4, 12, &result);
  if (rc != 0) {
    printf("%s: test #1 expect rc==0 but got %d\n", testName, rc);
    ++errCount;
  }

  if (result != 1) {
    printf("%s: test #1 expect result==1 but got %d\n", testName, result);
    ++errCount;
  }

  // this should pass, and result should be 0
  rc = checkChars(s, 4, 11, &result);
  if (rc != 0) {
    printf("%s: test #2 expect rc==0 but got %d\n", testName, rc);
    ++errCount;
  }

  if (result != 0) {
    printf("%s: test #2 expect result==0 but got %d\n", testName, result);
    ++errCount;
  }

  // now check behavior with invalid arguments
  rc = checkChars(s, 4, 110, &result);
  if (rc == 0) {
    printf("%s: test #3 expect rc!=0 but got %d\n", testName, rc);
    ++errCount;
  }

  // now check behavior with invalid arguments
  rc = checkChars(s, -129, 4, &result);
  if (rc == 0) {
    printf("%s: test #4 expect rc!=0 but got %d\n", testName, rc);
    ++errCount;
  }

  // and test an edge case
  rc = checkChars(s, 0, len-1, &result);
  if (rc != 0) {
    printf("%s: test #5 expect rc==0 but got %d\n", testName, rc);
    ++errCount;
  }

  if (result != 1) {
    printf("%s: test #5 expect result==1 but got %d\n", testName, result);
    ++errCount;
  }

  return errCount;
}

//--------------------------------------------------------------

int test_gcd() {
  const char *testName = __FUNCTION__;
  int rc, errCount, result;

  errCount = 0;

  // this should pass, and the result should be 3
  rc = gcd(57, 111, &result);
  if (rc != 0) {
    printf("%s: test #1 expect rc==0 but got %d\n", testName, rc);
    ++errCount;
  }

  if (result != 3) {
    printf("%s: test #1 expect result==3 but got %d\n", testName, result);
    ++errCount;
  }

  // this should fail
  rc = gcd(0, 111, &result);
  if (rc == 0) {
    printf("%s: test #2 expect rc!=0 but got %d\n", testName, rc);
    ++errCount;
  }

  // this should fail
  rc = gcd(12, -5, &result);
  if (rc == 0) {
    printf("%s: test #3 expect rc!=0 but got %d\n", testName, rc);
    ++errCount;
  }

  // an edge case
  rc = gcd(57, 1, &result);
  if (rc != 0) {
    printf("%s: test #4 expect rc==0 but got %d\n", testName, rc);
    ++errCount;
  }

  if (result != 1) {
    printf("%s: test #4 expect result==1 but got %d\n", testName, result);
    ++errCount;
  }

  return errCount;
}

//--------------------------------------------------------------


int main() {
  int rc, rtnval;

  rtnval = 0;

  rc = test_checkChars();
  if (rc != 0) {
    printf("test_checkChars failed\n");
    rtnval = 8;
  } else {
    printf("test_checkChars passed\n");
  }

  rc = test_gcd();
  if (rc != 0) {
    printf("test_gcd failed\n");
    rtnval = 8;
  } else {
    printf("test_gcd passed\n");
  }

  return rtnval;
}
