// jdh 6-8-22
// example of the use of gettimeofday() to compute elapsed time

#include <stdio.h>
#include <sys/time.h>

int f(int n) {
  int i, j;
  double r;
  // waste some time
  for (i=0; i<n; ++i) {
    for (j=0; j<n; ++j) {
      r = (double) i / (double) (i+j);
    }
  }
  return 0;
}

int main() {
  struct timeval t1, t2;
  float elapsedTime;

  gettimeofday(&t1, NULL);

  f(10000);

  gettimeofday(&t2, NULL);

  elapsedTime = (t2.tv_sec - t1.tv_sec) * 1000.0;    // sec to usec
  elapsedTime += (t2.tv_usec - t1.tv_usec) / 1000.0; // usec to ms
  printf("elapsed time: %f ms\n", elapsedTime);

  return 0;
}
