// jdh 6-7-22
// fgets() and sscanf() example

#include <stdio.h>
#include <stdlib.h>

#define BUFFLEN 1024

#define FILENAME "testOne.atrace.out"

int main() {
  char buffer[BUFFLEN];
  char tmpbuf[64];
  unsigned long val1, val2;
  int nf;
  FILE *fp;
  char *chp;

  fp = fopen(FILENAME, "r");
  if (fp == NULL) {
    printf("cannot open file '%s' for reading\n", FILENAME);
    return 8;
  }

  chp = fgets(buffer, BUFFLEN, fp);
  while ( chp != NULL ) {
    nf = sscanf(buffer, "%lp: %c %lp", &val1, tmpbuf, &val2);
    if (nf == 3) {
      printf("%s", buffer);
      printf("val1 = %lu; val2 = %lu\n", val1, val2);
    }
    chp = fgets(buffer, BUFFLEN, fp);
  }

  fclose(fp);

  return 0;
}

  

