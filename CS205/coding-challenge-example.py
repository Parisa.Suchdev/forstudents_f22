# 6-21-22
# an example of a coding challenge
#
# count how many elements of the first list are not in the second list,
# how many elements of the second list are not in the first list, and return
# these two counts as a list.  Can assume that there are no repeated values
# in either list

def compare_lists(list1, list2):
  count1 = 0
  count2 = 0

  # check items in first list: in second list?
  for elt in list1:
    if elt not in list2:
      count1 = count1 + 1

  # check items in second list: in first list?
  for elt in list2:
    if elt not in list1:
      count2 = count2 + 1

  return [count1, count2]

#-------------------------------------------------------
# basic test

def test_one():
  fail_flag = False

  list1 = ['ahem', 'bob', 'cat']
  list2 = ['bob', 'cat', 'dog', 'emu', 78]
  result = compare_lists(list1, list2)
  if result[0] != 1:
    print('error in test_one: expect 1 for first value')
    fail_flag = True
  if result[1] != 3:
    print('error in test_one: expect 2 for second value')
    fail_flag = True

  if fail_flag:
    print('FAILED test_one')
  else:
    print('PASSED test_one')

  return fail_flag

#-------------------------------------------------------
# edge-case test: second list is empty

def test_two():
  fail_flag = False

  list1 = ['a', 'b', 'c']
  list2 = []
  result = compare_lists(list1, list2)
  if result[0] != 3:
    print('error in test_two: expect 3 for first value')
    fail_flag = True
  if result[1] != 0:
    print('error in test_two: expect 0 for second value')
    fail_flag = True

  if fail_flag:
    print('FAILED test_two')
  else:
    print('PASSED test_two')

  return fail_flag

#-------------------------------------------------------
# edge-case test: the two lists are equal

def test_three():
  fail_flag = False

  list1 = ['a', 'b', 'c']
  result = compare_lists(list1, list1)
  if result[0] != 0:
    print('error in test_three: expect 0 for first value')
    fail_flag = True
  if result[1] != 0:
    print('error in test_three: expect 0 for second value')
    fail_flag = True

  if fail_flag:
    print('FAILED test_three')
  else:
    print('PASSED test_three')

  return fail_flag

#-------------------------------------------------------

def main():
  test_one()
  test_two()
  test_three()

#-------------------------------------------------------

main()
