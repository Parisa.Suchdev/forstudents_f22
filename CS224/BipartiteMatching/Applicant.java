// CS224 Fall 2022

public class Applicant {
  String name;

  public Applicant(String name) {
    this.name = name;
  }

  @Override
  public String toString() {
    return this.name;
  }
}
