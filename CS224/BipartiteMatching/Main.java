// CS224 Fall 2022

public class Main {
  public static void main(String args[]) {
    testOneA();
  }

  // this is Figure 7.9
  // there is no perfect matching
  public static void testOne() {
    Matching matching = new Matching();

    Applicant a1 = new Applicant("a1");
    Applicant a2 = new Applicant("a2");
    Applicant a3 = new Applicant("a3");
    Applicant a4 = new Applicant("a4");
    Applicant a5 = new Applicant("a5");

    matching.addToListOne(a1);
    matching.addToListOne(a2);
    matching.addToListOne(a3);
    matching.addToListOne(a4);
    matching.addToListOne(a5);

    Position p1 = new Position("p1");
    Position p2 = new Position("p2");
    Position p3 = new Position("p3");
    Position p4 = new Position("p4");
    Position p5 = new Position("p5");

    matching.addToListTwo(p1);
    matching.addToListTwo(p2);
    matching.addToListTwo(p3);
    matching.addToListTwo(p4);
    matching.addToListTwo(p5);

    matching.addConnection(a1, p1);
    matching.addConnection(a1, p3);
    matching.addConnection(a2, p1);
    matching.addConnection(a2, p2);
    matching.addConnection(a2, p3);
    matching.addConnection(a2, p4);
    matching.addConnection(a3, p4);
    matching.addConnection(a4, p4);
    matching.addConnection(a5, p4);
    matching.addConnection(a5, p5);

    boolean result = matching.solve();
    if ( ! result ) {
      System.out.println("no perfect matching exists");
    } else {
      System.out.println("perfect matching found!");
    }
  } // testOne()

  // this is Figure 7.9, with one additional edge
  // so that there is a perfect matching
  public static void testOneA() {
    Matching matching = new Matching();

    Applicant a1 = new Applicant("a1");
    Applicant a2 = new Applicant("a2");
    Applicant a3 = new Applicant("a3");
    Applicant a4 = new Applicant("a4");
    Applicant a5 = new Applicant("a5");

    matching.addToListOne(a1);
    matching.addToListOne(a2);
    matching.addToListOne(a3);
    matching.addToListOne(a4);
    matching.addToListOne(a5);

    Position p1 = new Position("p1");
    Position p2 = new Position("p2");
    Position p3 = new Position("p3");
    Position p4 = new Position("p4");
    Position p5 = new Position("p5");

    matching.addToListTwo(p1);
    matching.addToListTwo(p2);
    matching.addToListTwo(p3);
    matching.addToListTwo(p4);
    matching.addToListTwo(p5);

    matching.addConnection(a1, p1);
    matching.addConnection(a1, p3);
    matching.addConnection(a2, p1);
    matching.addConnection(a2, p2);
    matching.addConnection(a2, p3);
    matching.addConnection(a2, p4);
    matching.addConnection(a3, p4);
    matching.addConnection(a4, p4);
    matching.addConnection(a5, p4);
    matching.addConnection(a5, p5);

    // additional edge
    matching.addConnection(a4, p3);

    boolean result = matching.solve();
    if ( ! result ) {
      System.out.println("no perfect matching exists");
    } else {
      System.out.println("perfect matching found!");
    }
  } // testOneA
}
