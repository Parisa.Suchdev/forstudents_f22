// CS224 Fall 2022

import java.util.ArrayList;
import java.util.List;

public class Matching {
  private class Connection {
    Object o1, o2;
    private Connection(Object o1, Object o2) {
      this.o1 = o1;
      this.o2 = o2;
    }
  }

  //=====================================================

  List<Object> listOne;
  List<Object> listTwo;
  List<Connection> connections;

  public Matching() {
    listOne = new ArrayList<Object> ();
    listTwo = new ArrayList<Object> ();
    connections = new ArrayList<Connection> ();
  }

  //=====================================================

  public void addToListOne(Object o) {
    int idx = listOne.indexOf(o);
    if (idx < 0)
      listOne.add(o);
  }

  //=====================================================

  public void addToListTwo(Object o) {
    int idx = listTwo.indexOf(o);
    if (idx < 0)
      listTwo.add(o);
  }

  //=====================================================

  public void addConnection(Object o1, Object o2) {
    int idx1 = listOne.indexOf(o1);
    if (idx1 < 0) {
      System.out.println("ERROR: object " + o1 + " not in first list");
      return;
    }
    int idx2 = listTwo.indexOf(o2);
    if (idx2 < 0) {
      System.out.println("ERROR: object " + o2 + " not in second list");
      return;
    }

    Connection c = new Connection(o1, o2);
    connections.add(c);
  }

  //=====================================================

  public boolean solve() {

    // implement this

  } // solve()
}
