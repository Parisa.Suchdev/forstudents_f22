// CS224 Fall 2022

import java.util.ArrayList;
import java.util.List;

public class Node {
  int name;
  Object o;
  List<Edge> adjlist;

  public Node(int name, Object o) {
    this.name = name;
    this.o = o;
    this.adjlist = new ArrayList<Edge> ();
  }

  public void addEdge(Edge edge) {
    this.adjlist.add(edge);
  }

  @Override
  public String toString() {
    String s = "N" + name;
    return s;
  }
}
