import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
public class Main {
public static void main(String argv[]) {
testOne();
// testTwo();
}
// --------------------------------------------------------
// example of how to set up a testcase
//
// Frank GEOG010 GEOG011
// Cynthia GEOG010
// Walter GEOG101
// George GEOG101 GEOG201
//
// here is the solution (size = 2):
// Frank: GEOG010 GEOG011
// George: GEOG101 GEOG201
public static void testOne() {
Instructor frank = new Instructor("Frank");
String frankCourses[] = { "GEOG010", "GEOG011" };
frank.teach(frankCourses);
Instructor cynthia = new Instructor("Cynthia");
String cynthiaCourses[] = { "GEOG010" };
cynthia.teach(cynthiaCourses);
Instructor walter = new Instructor("Walter");
String walterCourses[] = { "GEOG101" };
walter.teach(walterCourses);
Instructor george = new Instructor("George");
String georgeCourses[] = { "GEOG101", "GEOG201" };
george.teach(georgeCourses);
Instructor[] instructors = { frank, cynthia, walter, george };
String[] courses = { "GEOG010", "GEOG011", "GEOG101", "GEOG201" };
Instructor[] coverSet = findMinCover(courses, instructors);
for (int i = 0; i < coverSet.length; ++i)
System.out.println(coverSet[i]);
}
// -----------------------------------------------------------------------
// Write this testcase to mode the example from the assignment spec
//
// here's the unique solution (of size = 4):
// John: ART002 ART008 ART125
// Betsy: ART124 ART125 ART201
// Hiram: ART001 ART110 ART125
// Ralph: ART008 ART064 ART205 ART266
public static void testTwo() {
// fill this in
Instructor[] coverSet = findMinCover(courses, instructors);
for (int i = 0; i < coverSet.length; ++i)
System.out.println(coverSet[i]);
}
// -----------------------------------------------------------------------
public static Instructor[] findMinCover(String[] courses, Instructor[] instructors) {
// It should return an array consisting of the smallest number of instructors
// necessary so that each course has
// an instructor who can teach it.
// The function testOne() in Main.java shows how to set up a schedule and call
// findMinCover().
// call permute for each instructor
// add to arraylist
// go through arraylist and find the least amount of professors to cover all of
// the courses.
// courses array used to doc this
// compare to courses to make sure all of the strings are checked off
// return the array smallteach of the smallest number of teachers whos can teach
// every class
ArrayList<boolean[]> possibilities = permute(instructors.length);
Instructor[] instructors_used = new Instructor[instructors.length];
String[] courses_used = new String[courses.length];
boolean[] smallest_num = new boolean[instructors.length];
Instructor[] teachers = new Instructor[instructors.length];
for (boolean[] array : possibilities) {
int include = 0;
int smallest = 0;
for (int i = 0; i < array.length; i++) {
if (array[i] == true) {
instructors_used[i] = instructors[i];
}
}
for (String c : courses) {
for (int i = 0; i < instructors_used.length; i++) {
if (instructors_used[i].canTeach(c)) {
for (int j = 0; j < courses_used.length; j++) {
courses_used[j] = c;
}
}
}
}
for (int i = 0; i < instructors.length; i++) {
// And this is where i got stumped. I, along with my partner, could not figure
// out how to collect and compare the courses that can be taught
// in a way that doesnt use a function such as a .add() function
if (instructors_used[i] != null) {
include++;
}
}
}
return smallest_num;
}
// -----------------------------------------------------------------------
public static ArrayList<boolean[]> permute(int n) {
// Write this function: ArrayList<boolean[]> permute(int n). This will return 2
// n arrays, each of length n,
// with all possible combinations of true and false. For example, permute(3)
// will return an ArrayList containing these eight arrays: [false, false,
// false], [false, false, true], [false, true, false],
// [false, true, true], [true, false, false], [true, false, true], [true, true,
// false], [true,
// true, true]
// initialize a new ArrayList<boolean[]> rtnval
// this will be the return value of the function
ArrayList<boolean[]> rtnval = new ArrayList<boolean[]>();
if (n == 0) {
// create a new boolean[] array of length = 0
boolean[] noneArray = new boolean[0];
// add this array to rtnval
rtnval.add(noneArray);
} else {
ArrayList<boolean[]> sublist = new ArrayList<boolean[]>();
sublist = permute(n - 1);
// for each element e of sublist
for (boolean[] e : sublist) {
// create a new boolean[] array a1 of length n
boolean[] a1 = new boolean[n];
// copy e to a1
a1 = Arrays.copyOf(e, n);
// set a1[n-1] to true
a1[n - 1] = true;
// add a1 to rtnval
rtnval.add(a1);
// create a new boolean[] array a2 of length n
boolean[] a2 = new boolean[n];
// copy e to a2
a2 = Arrays.copyOf(e, n);
// set a2[n-1] to false
a2[n - 1] = false;
// add a2 to rtnval
rtnval.add(a2);
}
return rtnval;
}
}
}
