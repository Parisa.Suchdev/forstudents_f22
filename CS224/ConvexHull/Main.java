// CS224 Fall 2022

import java.util.List;
import java.util.ArrayList;

public class Main {
  public static void main(String args[]) {
    runTests();
//  testOne();
//  testTwo();
  }

  public static void runTests() {
    PointCollection points = new PointCollection();
    points.runTests();
  }

  public static void testOne() {
    PointCollection points = new PointCollection();

    points.addPoint( new Point(4, 4) );
    points.addPoint( new Point(8, 4) );
    points.addPoint( new Point(11, 3) );
    points.addPoint( new Point(12, 1) );
    points.addPoint( new Point(7, 3) );
    points.addPoint( new Point(2, 3) );
    points.addPoint( new Point(1, 1) );
    points.addPoint( new Point(5, 2) );
    points.addPoint( new Point(6, 2) );
    points.addPoint( new Point(4, 0) );

    List<Point> ch = points.convexHull();
    for (Point p: ch) {
      System.out.print(p + " ");
    }
    System.out.println();
  } // testOne()

  public static void testTwo() {
    PointCollection points = new PointCollection();

    points.addPoint( new Point(4, 4) );
    points.addPoint( new Point(8, 4) );
    points.addPoint( new Point(11, 3) );
    points.addPoint( new Point(12, 1) );
    points.addPoint( new Point(7, 3) );
    points.addPoint( new Point(2, 3) );
    points.addPoint( new Point(1, 1) );
    points.addPoint( new Point(5, 2) );
    points.addPoint( new Point(6, 2) );
    points.addPoint( new Point(4, 0) );
    // additional points in the interior
    points.addPoint( new Point(2, 2) );
    points.addPoint( new Point(3, 1) );
    points.addPoint( new Point(8, 1) );
    points.addPoint( new Point(9, 1) );
    points.addPoint( new Point(10, 2) );

    List<Point> ch = points.convexHull();
    for (Point p: ch) {
      System.out.print(p + " ");
    }
    System.out.println();
  } // testTwo()
}
