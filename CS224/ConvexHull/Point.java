// CS224 F22

public class Point {
  int x;
  int y;

  public Point(int x, int y) {
    this.x = x;
    this.y = y;
  }

  @Override
  public String toString() {
    String s = "(" + this.x + ", " + this.y + ")";
    return s;
  }

  @Override
  public boolean equals(Object o) {
    if (o == this)
      return true;

    if ( ! (o instanceof Point) )
      return false;

    Point other = (Point) o;

    return other.x == this.x && other.y == this.y;
  }

  public int compareX(Object o) {
   Point otherPoint = (Point) o;
   if (this.x < otherPoint.x)
     return -1;
   else if (this.x > otherPoint.x)
     return 1;
   else
     return 0;
  }

  public int compareY(Object o) {
   Point otherPoint = (Point) o;
   if (this.y < otherPoint.y)
     return -1;
   else if (this.y > otherPoint.y)
     return 1;
   else
     return 0;
  }
}
