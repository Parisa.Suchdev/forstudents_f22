// CS224 Fall 2022

import java.util.List;
import java.util.ArrayList;

class PointCollection {
  List<Point> points;

  public PointCollection() {
    points = new ArrayList<Point>();
  }

  public void addPoint(Point p) {
    points.add(p);
  }

  private static List<Point> clockwiseOrder(List<Point> points) {
    // put three or fewer points in clockwise order;
    // assume that they are already sorted by x-coordinate

    // implement this function

  } // clockwiseOrder()

  //-----------------------------------------------------

  private static int crossProduct(Point p1, Point p2) {
    return p1.x * p2.y - p2.x * p1.y;
  }

  //-----------------------------------------------------

  private static int direction(Point p1, Point p2, Point p3) {

    // implement this function

  } // direction()

  //-----------------------------------------------------

  private List<Point> merge(List<Point> C1, List<Point> C2) {

    // implement this function

  } // merge()

  //-----------------------------------------------------

  private List<Point> convexHullRec(List<Point> points) {

    // implement this function

  } // convexHullRec()

  //---------------------------------------------------------

  public List<Point> convexHull() {

    // implement this function

  } // convexHull()

  //---------------------------------------------------------

  private boolean  testDirection() {
    boolean fail = false;

    Point p1 = new Point(1, 1);
    Point p2 = new Point(2, 0);
    Point p3 = new Point(4, 4);
    int val = PointCollection.direction(p1, p2, p3);
    // val should be negative
    if (val >= 0) {
      System.out.println("ERROR test #1 failed in test_direction");
      fail = true;
    }

    p1 = new Point(0, 0);
    p2 = new Point(1, 1);
    p3 = new Point(6, -10);
    val = PointCollection.direction(p1, p2, p3);
    // val should be positive
    if (val <= 0) {
      System.out.println("ERROR test #2 failed in test_direction");
      fail = true;
    }

    p1 = new Point(4, 4);
    p2 = new Point(3, 3);
    p3 = new Point(10, 2);
    val = PointCollection.direction(p1, p2, p3);
    // val should be negative
    if (val >= 0) {
      System.out.println("ERROR test #5 failed in test_direction");
      fail = true;
    }

    p1 = new Point(4, 4);
    p2 = new Point(3, 3);
    p3 = new Point(-8, 1);
    val = PointCollection.direction(p1, p2, p3);
    // val should be positive
    if (val <= 0) {
      System.out.println("ERROR test #6 failed in test_direction");
      fail = true;
    }

    if ( ! fail )
      System.out.println("PASSED test direction");

    return fail;
  } // testDirection()

  //---------------------------------------------------------

  private boolean testOrder() {
    int testNum = 1;
    boolean fail = false;

    // first test: distinct x and y coords
    Point p1 = new Point(1, 1);
    Point p2 = new Point(4, 0);
    Point p3 = new Point(2, 3);
    List<Point> points = new ArrayList<Point> ();
    points.add(p1);
    points.add(p2);
    points.add(p3);
    // clockwise should be p1, p3, p2
    List<Point> ordered = PointCollection.clockwiseOrder(points);

    if (ordered.size() != 3) {
      System.out.println("ERROR: wrong # points returned by clockwiseOrder() in test " + testNum);
      fail = true;
    }

    if (ordered.get(0) == p1) {
      if (ordered.get(1) != p3 || ordered.get(2) != p2) {
        System.out.println("ERROR: wrong order returned by clockwiseOrder() in test " + testNum);
        fail = true;
      }
    } else if (ordered.get(0) == p3) {
      if (ordered.get(1) != p2 || ordered.get(2) != p1) {
        System.out.println("ERROR: wrong order returned by clockwiseOrder() in test " + testNum);
        fail = true;
      }
    } else if (ordered.get(0) == p2) {
      if (ordered.get(1) != p1 || ordered.get(2) != p3) {
        System.out.println("ERROR: wrong order returned by clockwiseOrder() in test " + testNum);
        fail = true;
      }
    }

    // second test: p1 and p2 have same x coord
    testNum = 2;
    p1 = new Point(4, 3);
    p2 = new Point(4, 6);
    p3 = new Point(3, 9);
    points = new ArrayList<Point> ();
    points.add(p1);
    points.add(p2);
    points.add(p3);
    // clockwise should be p1, p3, p2
    ordered = PointCollection.clockwiseOrder(points);

    if (ordered.size() != 3) {
      System.out.println("ERROR: wrong # points returned by clockwiseOrder() in test " + testNum);
      fail = true;
    }

    if (ordered.get(0) == p1) {
      if (ordered.get(1) != p3 || ordered.get(2) != p2) {
        System.out.println("ERROR: order returned by clockwiseOrder() in test " + testNum);
        fail = true;
      }
    } else if (ordered.get(0) == p3) {
      if (ordered.get(1) != p2 || ordered.get(2) != p1) {
        System.out.println("ERROR: order returned by clockwiseOrder() in test " + testNum);
        fail = true;
      }
    } else if (ordered.get(0) == p2) {
      if (ordered.get(1) != p1 || ordered.get(2) != p3) {
        System.out.println("ERROR: order returned by clockwiseOrder() in test " + testNum);
        fail = true;
      }
    }

    // third test: p1 and p3 have same y coord
    testNum = 3;
    p1 = new Point(4, 3);
    p2 = new Point(5, 1);
    p3 = new Point(10, 3);
    points = new ArrayList<Point> ();
    points.add(p1);
    points.add(p2);
    points.add(p3);
    // clockwise should be p1, p3, p2
    ordered = PointCollection.clockwiseOrder(points);

    if (ordered.size() != 3) {
      System.out.println("ERROR: wrong # points returned by clockwiseOrder() in test " + testNum);
      fail = true;
    }

    if (ordered.get(0) == p1) {
      if (ordered.get(1) != p3 || ordered.get(2) != p2) {
        System.out.println("ERROR: order returned by clockwiseOrder() in test " + testNum);
        fail = true;
      }
    } else if (ordered.get(0) == p3) {
      if (ordered.get(1) != p2 || ordered.get(2) != p1) {
        System.out.println("ERROR: order returned by clockwiseOrder() in test " + testNum);
        fail = true;
      }
    } else if (ordered.get(0) == p2) {
      if (ordered.get(1) != p1 || ordered.get(2) != p3) {
        System.out.println("ERROR: order returned by clockwiseOrder() in test " + testNum);
        fail = true;
      }
    }
    System.out.println("PASSED test clockwiseOrder()");
    return true;
  }

  //---------------------------------------------------------

  public void runTests() {
    testOrder();
    testDirection();
  }

} // class PointCollection
