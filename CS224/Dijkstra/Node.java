// CS224 Fall 2022

import java.util.ArrayList;
import java.lang.Comparable;

public class Node {
  ArrayList<Link> adjlistOut;
  ArrayList<Link> adjlistIn;
  int name;

  public Node(int name) {
    this.name = name;
    this.adjlistOut = new ArrayList<Link>();
    this.adjlistIn = new ArrayList<Link>();
  }

  public void addTo(Node toNode, int weight) {
    Link outEdge = new Link(toNode, weight);
    this.adjlistOut.add(outEdge);
  }

  public void addFrom(Node fromNode, int weight) {
    Link inEdge = new Link(fromNode, weight);
    this.adjlistIn.add(inEdge);
  }

  public String toString() {
    String s = "N " + this.name;
    return s;
  }
}
